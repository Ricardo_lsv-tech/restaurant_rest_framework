from django import forms
from rest_app.models import Client, Invoice, Order, Stock, Table, Waiter

class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ('name', 'last_name', 'observations')

    name = forms.CharField(max_length=45, widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(max_length=45, widget=forms.TextInput(attrs={'class': 'form-control'}))
    observations = forms.CharField(max_length=500, widget=forms.Textarea(attrs={'class': 'form-control'}))

class WaiterForm(forms.ModelForm):
    class Meta:
        model = Waiter
        fields = ('name', 'first_last_name', 'second_last_name')

    name = forms.CharField(max_length=45, widget=forms.TextInput(attrs={'class': 'form-control'}))
    first_last_name = forms.CharField(max_length=45, widget=forms.TextInput(attrs={'class': 'form-control'}))
    second_last_name = forms.CharField(max_length=45, widget=forms.TextInput(attrs={'class': 'form-control'}))

class StockForm(forms.ModelForm):
    class Meta:
        model = Stock
        fields = ('name', 'type', 'importe')

    name = forms.CharField(max_length=45, widget=forms.TextInput(attrs={'class': 'form-control'}))
    type = forms.ChoiceField(choices=(('None', '-----'), ('meat', 'Meat'), ('drink', 'Drink')), widget=forms.Select(attrs={'class': 'form-control'}))
    importe = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))

class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ('id_stock', 'quantity')

    id_stock = forms.ModelChoiceField(queryset=Stock.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}))
    quantity = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))

class TableForm(forms.ModelForm):
    class Meta:
        model = Table
        fields = ('zone', 'num_diners')

    zone = forms.CharField(max_length=45, widget=forms.TextInput(attrs={'class': 'form-control'}))
    num_diners = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))

class InvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = ('id_table', 'id_waiter', 'id_client', 'id_order', 'id_stock')

    id_table = forms.ModelChoiceField(queryset=Table.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}))
    id_waiter = forms.ModelChoiceField(queryset=Waiter.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}))
    id_client = forms.ModelChoiceField(queryset=Client.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}))
    id_order = forms.ModelChoiceField(queryset=Order.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}))
    id_stock = forms.ModelChoiceField(queryset=Stock.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}))