
from django.contrib import admin
from .models import Client, Invoice, Order, Stock, Table, UserToken, Waiter

@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'last_name', 'observations')
    search_fields = ('name', 'last_name', 'observations')

@admin.register(Waiter)
class WaiterAdmin(admin.ModelAdmin):
    list_display = ('name', 'first_last_name', 'second_last_name')
    search_fields = ('name', 'first_last_name', 'second_last_name')

@admin.register(Stock)
class StockAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'importe')
    search_fields = ('name', 'type', 'importe')

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id_stock', 'quantity')
    search_fields = ('id_stock', 'quantity')

@admin.register(Table)
class TableAdmin(admin.ModelAdmin):
    list_display = ('zone', 'num_diners')
    search_fields = ('zone', 'num_diners')

@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_display = ('id_invoice', 'id_table', 'id_waiter', 'id_client', 'id_order', 'id_stock')
    search_fields = ('id_invoice', 'id_waiter', 'id_client')

@admin.register(UserToken)
class UserTokenAdmin(admin.ModelAdmin):
    list_display = ('username', 'token')
    search_fields = ('username', 'token')