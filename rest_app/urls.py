from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.decorators import login_required
from rest_app.views import (IndexView, 
                            ClientView, CreateClientView, UpdateClientView, DeleteClientView,
                            WaiterView, CreateWaiterView, UpdateWaiterView, DeleteWaiterView,
                            TableView, CreateTableView, UpdateTableView, DeleteTableView,
                            StockView, CreateStockView, UpdateStockView, DeleteStockView,
                            OrderView, CreateOrderView, UpdateOrderView, DeleteOrderView,
                            InvoiceView, CreateInvoiceView, UpdateInvoiceView, DeleteInvoiceView)

urlpatterns = [
    path('', login_required(IndexView.as_view()), name='index'),
    path('client_list/', login_required(ClientView.as_view()), name='client_list'),
    path('create_client/', login_required(CreateClientView.as_view()), name='create_client'),
    path('update_client/<int:pk>/', login_required(UpdateClientView.as_view()), name='update_client'),
    path('delete_client/<int:pk>/', login_required(DeleteClientView.as_view()), name='delete_client'),

    path('waiter_list/', login_required(WaiterView.as_view()), name='waiter_list'),
    path('create_waiter/', login_required(CreateWaiterView.as_view()), name='create_waiter'),
    path('update_waiter/<int:pk>/', login_required(UpdateWaiterView.as_view()), name='update_waiter'),
    path('delete_waiter/<int:pk>/',login_required( DeleteWaiterView.as_view()), name='delete_waiter'),
    
    path('table_list/', login_required(TableView.as_view()), name='table_list'),
    path('create_table/', login_required(CreateTableView.as_view()), name='create_table'),
    path('update_table/<int:pk>/', login_required(UpdateTableView.as_view()), name='update_table'),
    path('delete_table/<int:pk>/', login_required(DeleteTableView.as_view()), name='delete_table'),

    path('stock_list/', login_required(StockView.as_view()), name='stock_list'),
    path('create_stock/', login_required(CreateStockView.as_view()), name='create_stock'),
    path('update_stock/<int:pk>/', login_required(UpdateStockView.as_view()), name='update_stock'),
    path('delete_stock/<int:pk>/', login_required(DeleteStockView.as_view()), name='delete_stock'),

    path('order_list/', login_required(OrderView.as_view()), name='order_list'),
    path('create_order/', login_required(CreateOrderView.as_view()), name='create_order'),
    path('update_order/<int:pk>/', login_required(UpdateOrderView.as_view()), name='update_order'),
    path('delete_order/<int:pk>/', login_required(DeleteOrderView.as_view()), name='delete_order'),

    path('invoice_list/', login_required(InvoiceView.as_view()), name='invoice_list'),
    path('create_invoice/', login_required(CreateInvoiceView.as_view()), name='create_invoice'),
    path('update_invoice/<int:pk>/', login_required(UpdateInvoiceView.as_view()), name='update_invoice'),
    path('delete_invoice/<int:pk>/', login_required(DeleteInvoiceView.as_view()), name='delete_invoice'),

    path('accounts/login/', LoginView.as_view(template_name= 'login/index.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
]