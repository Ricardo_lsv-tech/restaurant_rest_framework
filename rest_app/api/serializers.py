
from rest_app.models import Client, Invoice, Order, Stock, Table, Waiter
from rest_framework import serializers


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id_client', 'name', 'last_name', 'observations']

class StockSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stock
        fields = ['id_stock', 'name', 'type', 'importe']

class OrderSerializer(serializers.ModelSerializer):
    id_stock = serializers.StringRelatedField()
    class Meta:
        model = Order
        fields = ['id_order', 'id_stock', 'quantity']

class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Table
        fields = ['id_table', 'zone', 'num_diners']

class InvoiceSerializer(serializers.ModelSerializer):
    id_client = serializers.StringRelatedField()
    id_waiter = serializers.StringRelatedField()
    id_stock = serializers.StringRelatedField()
    id_order = serializers.StringRelatedField()
    id_table = serializers.StringRelatedField()

    class Meta:
        model = Invoice
        fields = ['id_invoice', 'id_table', 'id_waiter', 'id_client', 'id_order', 'id_stock']

class WaiterSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Waiter
        fields = ['id_waiter', 'name', 'first_last_name', 'second_last_name']