from django.urls import path, include
from rest_framework.routers import DefaultRouter

from rest_app.api.views import (ClientViewSet, InvoiceViewSet,
                                OrderViewSet, StockViewSet,
                                TableViewSet, WaiterViewSet)

router = DefaultRouter()
router.register(r'clients', ClientViewSet)
router.register(r'invoices', InvoiceViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'stocks', StockViewSet)
router.register(r'tables', TableViewSet)
router.register(r'waiters', WaiterViewSet)



urlpatterns = [
    path('', include(router.urls)),
    #path('client', ClientViewSet.as_view(), name='client_api'),
    #path('waiter', WaiterViewSet.as_view(), name='waiter_api'),
    #path('invoice', InvoiceViewSet.as_view(), name='invoice_api'),
    #path('order', OrderViewSet.as_view(), name='order_api'),
    #path('table', TableViewSet.as_view(), name='table_api'),
    #path('stock', StockViewSet.as_view(), name='stock_api'),
]