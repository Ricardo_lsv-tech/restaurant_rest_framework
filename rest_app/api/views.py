from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from rest_app.models import Client, Invoice, Order, Stock, Table, Waiter
from rest_app.api.serializers import (ClientSerializer, StockSerializer,
                                        OrderSerializer, TableSerializer,
                                        InvoiceSerializer, WaiterSerializer)

class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permissions_classes = [IsAuthenticated]

class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    permissions_classes = [IsAuthenticated]
class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permissions_classes = [IsAuthenticated]
class WaiterViewSet(viewsets.ModelViewSet):
    queryset = Waiter.objects.all()
    serializer_class = WaiterSerializer
    permissions_classes = [IsAuthenticated]
class TableViewSet(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer
    permissions_classes = [IsAuthenticated]
class StockViewSet(viewsets.ModelViewSet):
    queryset = Stock.objects.all()
    serializer_class = StockSerializer
    permissions_classes = [IsAuthenticated]