from django.conf import settings
from django.http import HttpResponseRedirect
import requests
from django.shortcuts import redirect, render, resolve_url
from django.urls import reverse_lazy
from django.views import View
from django.contrib.auth.views import LoginView, LogoutView

from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from rest_app.forms import ClientForm, InvoiceForm, OrderForm, StockForm, TableForm, WaiterForm
from rest_app.models import Client, Invoice, Order, Stock, Table, Waiter, UserToken

# Create your views here.



class IndexView(View):
    def get(self, request):
        UserToken.objects.all().delete()
        try:
            user=self.request.user
            user_data={
                'username': str(user),
                'password': 'rika2022'
            }
            resp_token=requests.post('http://localhost:8000/api/token/', user_data)
            token=resp_token.json()['access']
            UserToken.objects.update_or_create(username=str(self.request.user), token=token)
            return render(request, 'index.html')
        except:
            print('aun no hay token')
        


#crud Clients
class ClientView(ListView):
    model = Client
    template_name = './clients/client_list.html'
    context_object_name = 'clients'
    

    def get_queryset(self):
        try:
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            resp = requests.get('http://localhost:8000/api/clients/', headers={'Authorization': 'Bearer' +token})
            data= resp.json()
            return data['results']
        except Exception as e:
            print(e)
        

class CreateClientView(CreateView):
    model = Client
    template_name = './clients/create_client.html'
    form_class = ClientForm
    success_url = reverse_lazy('client_list')

    def post(self, *args, **kwargs):
        form = ClientForm(self.request.POST)
        if form.is_valid():
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            requests.post('http://localhost:8000/api/clients/', data=form.cleaned_data, headers={'Authorization': 'Bearer' +token})
            return redirect(reverse_lazy('client_list'))
        else:
            return self.form_invalid(form)



class UpdateClientView(UpdateView):
    model = Client
    template_name = './clients/update_client.html'
    form_class = ClientForm
    success_url = reverse_lazy('client_list')

    def put(self, *args, **kwargs):
        form = ClientForm(self.request.POST)
        if form.is_valid():
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            requests.put(f"http://localhost:8000/api/clients/{self.kwargs['pk']}", data=form.cleaned_data, headers={'Authorization': 'Bearer' +token})
            return redirect(reverse_lazy('client_list'))
        else:
            return self.form_invalid(form)

class DeleteClientView(DeleteView):
    model = Client
    template_name = './clients/delete_client.html'
    success_url = reverse_lazy('client_list')

#crud Waiters
class WaiterView(ListView):
    model = Waiter
    template_name = './waiters/waiter_list.html'
    context_object_name = 'waiters'

    def get_queryset(self):
        try:
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            resp = requests.get('http://localhost:8000/api/waiters/', headers={'Authorization': 'Bearer' +token})
            data= resp.json()
            return data['results']
        except Exception as e:
            print(e)

class CreateWaiterView(CreateView):
    model = Waiter
    template_name = './waiters/create_waiter.html'
    form_class = WaiterForm
    success_url = reverse_lazy('waiter_list')

    def post(self, *args, **kwargs):
        form = WaiterForm(self.request.POST)
        if form.is_valid():
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            requests.post('http://localhost:8000/api/waiters/', data=form.cleaned_data, headers={'Authorization': 'Bearer' +token})
            return redirect(reverse_lazy('waiter_list'))
        else:
            return self.form_invalid(form)

class UpdateWaiterView(UpdateView):
    model = Waiter
    template_name = './waiters/update_waiter.html'
    form_class = WaiterForm
    success_url = reverse_lazy('waiter_list')

    def put(self, request, *args, **kwargs):
        form = WaiterForm(request.POST)
        if form.is_valid():
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            requests.put(f"http://localhost:8000/api/waiters/{self.kwargs['pk']}", data=form.cleaned_data, headers={'Authorization': 'Bearer' +token})
            return redirect(reverse_lazy('waiter_list'))
        else:
            return self.form_invalid(form)

class DeleteWaiterView(DeleteView):
    model = Waiter
    template_name = './waiters/delete_waiter.html'
    success_url = reverse_lazy('waiter_list')

#crud Tables
class TableView(ListView):
    model = Table
    template_name = './tables/table_list.html'
    context_object_name = 'tables'

    def get_queryset(self):
        try:
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            resp = requests.get('http://localhost:8000/api/tables/', headers={'Authorization': 'Bearer' +token})
            data= resp.json()
            return data['results']
        except Exception as e:
            print(e)

class CreateTableView(CreateView):
    model = Table
    template_name = './tables/create_table.html'
    form_class = TableForm
    success_url = reverse_lazy('table_list')

    def post(self, *args, **kwargs):
        form = TableForm(self.request.POST)
        if form.is_valid():
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            requests.post('http://localhost:8000/api/tables/', data=form.cleaned_data, headers={'Authorization': 'Bearer' +token})
            return redirect(reverse_lazy('table_list'))
        else:
            return self.form_invalid(form)

class UpdateTableView(UpdateView):
    model = Table
    template_name = './tables/update_table.html'
    form_class = TableForm
    success_url = reverse_lazy('table_list')

    def put(self, *args, **kwargs):
        form = TableForm(self.request.POST)
        if form.is_valid():
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            requests.put(f"http://localhost:8000/api/tables/{self.kwargs['pk']}", data=form.cleaned_data, headers={'Authorization': 'Bearer' +token})
            return redirect(reverse_lazy('table_list'))
        else:
            return self.form_invalid(form)

class DeleteTableView(DeleteView):
    model = Table
    template_name = './tables/delete_table.html'
    success_url = reverse_lazy('table_list')

#crud Stocks
class StockView(ListView):
    model = Stock
    template_name = './stocks/stock_list.html'
    context_object_name = 'stocks'

    def get_queryset(self):
        try:
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            resp = requests.get('http://localhost:8000/api/stocks/', headers={'Authorization': 'Bearer' +token})
            data= resp.json()
            return data['results']
        except Exception as e:
            print(e)

class CreateStockView(CreateView):
    model = Stock
    template_name = './stocks/create_stock.html'
    form_class = StockForm
    success_url = reverse_lazy('stock_list')

    def post(self, *args, **kwargs):
        form = StockForm(self.request.POST)
        if form.is_valid():
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            requests.post('http://localhost:8000/api/stocks/', data=form.cleaned_data, headers={'Authorization': 'Bearer' +token})
            return redirect(reverse_lazy('stock_list'))
        else:
            return self.form_invalid(form)

    

class UpdateStockView(UpdateView):
    model = Stock
    template_name = './stocks/update_stock.html'
    form_class = StockForm
    success_url = reverse_lazy('stock_list')

    def put(self, *args, **kwargs):
        form = StockForm(self.request.POST)
        if form.is_valid():
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            requests.put(f"http://localhost:8000/api/stocks/{self.kwargs['pk']}", data=form.cleaned_data, headers={'Authorization': 'Bearer' +token})
            return redirect(reverse_lazy('stock_list'))
        else:
            return self.form_invalid(form)

class DeleteStockView(DeleteView):
    model = Stock
    template_name = './stocks/delete_stock.html'
    success_url = reverse_lazy('stock_list')

#crud Orders
class OrderView(ListView):
    model = Order
    template_name = './orders/order_list.html'
    context_object_name = 'orders'

    def get_queryset(self):
        try:
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            resp = requests.get('http://localhost:8000/api/orders/', headers={'Authorization': 'Bearer' +token})
            data= resp.json()
            return data['results']
        except Exception as e:
            print(e)

class CreateOrderView(CreateView):
    model = Order
    template_name = './orders/create_order.html'
    form_class = OrderForm
    success_url = reverse_lazy('order_list')

    def post(self, *args, **kwargs):
        form = OrderForm(self.request.POST)
        if form.is_valid():
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            data=self.request.POST.dict()
            requests.post('http://localhost:8000/api/orders/', data=data, headers={'Authorization': 'Bearer' +token})
            return redirect(reverse_lazy('order_list'))
        else:
            return self.form_invalid(form)

class UpdateOrderView(UpdateView):
    model = Order
    template_name = './orders/update_order.html'
    form_class = OrderForm
    success_url = reverse_lazy('order_list')

    def put(self, *args, **kwargs):
        form = OrderForm(self.request.POST)
        if form.is_valid():
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            requests.put(f"http://localhost:8000/api/orders/{self.kwargs['pk']}", data=form.cleaned_data, headers={'Authorization': 'Bearer' +token})
            return redirect(reverse_lazy('order_list'))
        else:
            return self.form_invalid(form)

class DeleteOrderView(DeleteView):
    model = Order
    template_name = './orders/delete_order.html'
    success_url = reverse_lazy('order_list')

#crud Invoices
class InvoiceView(ListView):
    model = Invoice
    template_name = './invoices/invoice_list.html'
    context_object_name = 'invoices'

    def get_queryset(self):
        try:
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            resp = requests.get('http://localhost:8000/api/invoices/', headers={'Authorization': 'Bearer' +token})
            data= resp.json()
            return data['results']
        except Exception as e:
            print(e)

class CreateInvoiceView(CreateView):
    model = Invoice
    template_name = './invoices/create_invoice.html'
    form_class = InvoiceForm
    success_url = reverse_lazy('invoice_list')

    def post(self, *args, **kwargs):
        form = InvoiceForm(self.request.POST)
        if form.is_valid():
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            data=self.request.POST.dict()
            requests.post('http://localhost:8000/api/invoices/', data=data, headers={'Authorization': 'Bearer' +token})
            return redirect(reverse_lazy('invoice_list'))
        else:
            return self.form_invalid(form)

class UpdateInvoiceView(UpdateView):
    model = Invoice
    template_name = './invoices/update_invoice.html'
    form_class = InvoiceForm
    success_url = reverse_lazy('invoice_list')

    def put(self, *args, **kwargs):
        form = InvoiceForm(self.request.POST)
        if form.is_valid():
            token=UserToken.objects.filter(username=str(self.request.user))[0].token
            requests.put(f"http://localhost:8000/api/invoices/{self.kwargs['pk']}", data=form.cleaned_data, headers={'Authorization': 'Bearer' +token})
            return redirect(reverse_lazy('invoice_list'))
        else:
            return self.form_invalid(form)

class DeleteInvoiceView(DeleteView):
    model = Invoice
    template_name = './invoices/delete_invoice.html'
    success_url = reverse_lazy('invoice_list')


