from django.db import models

# Create your models here.
class Client(models.Model):
    id_client = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    observations = models.TextField(blank=True, null=True)

    def __str__(self):
        return f'{self.name} {self.last_name}'

class Waiter(models.Model):
    id_waiter = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45)
    first_last_name = models.CharField(max_length=45)
    second_last_name = models.CharField(max_length=45)

    def __str__(self):
        return f'{self.name} {self.first_last_name} {self.second_last_name}'

class Stock(models.Model):
    id_stock = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45)
    type = models.CharField(max_length=45)
    importe = models.IntegerField()

    def __str__(self):
        return f'{self.name}'

class Order(models.Model):
    id_order = models.AutoField(primary_key=True)
    id_stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    quantity = models.IntegerField()

    def __str__(self):
        return f'{self.id_stock} {self.quantity}'
   

class Table(models.Model):
    id_table = models.AutoField(primary_key=True)
    zone = models.CharField(max_length=45)
    num_diners = models.IntegerField()

    def __str__(self):
        return f'{self.zone}'

class Invoice(models.Model):
    id_invoice = models.AutoField(primary_key=True)
    id_table = models.ForeignKey(Table, on_delete=models.CASCADE)
    id_waiter = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    id_client = models.ForeignKey(Client, on_delete=models.CASCADE)
    id_order = models.ForeignKey(Order, on_delete=models.CASCADE)
    id_stock = models.ForeignKey(Stock, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.id_invoice}'

class UserToken(models.Model):
    token = models.CharField(max_length=255, primary_key=True)
    username = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.username}'