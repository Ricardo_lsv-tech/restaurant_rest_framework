### RESTAURANT

### Introduction

RESTAURANT, is an application for taking orders in restaurants around the world, with the ease of immediate delivery of invoices without as much delay or waiting as is usually the case in said restaurants.

# Installation

- ptyhon3 -m venv env_name
- source env_name/bin/activate
- git init (if you don't have git installed)
- git clone git@gitlab.com:Ricardo_lsv-tech/restaurant_rest_framework.git (by ssh)
- git clone https://gitlab.com/Ricardo_lsv-tech/restaurant_rest_framework.git (by http)
- cd restaurant
- pip install -r requirements.txt
- python manage.py makemigrations
- python manage.py migrate
- python manage.py createsuperuser
- python manage.py runserver

## MEMBERS

1. **RICARDO ENRIQUE JARAMILLO ACEVEDO**
